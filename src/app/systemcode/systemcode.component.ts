import {Component, OnInit} from '@angular/core';
import {MachineService} from '../service/machine/machine.service';
import {SystemCode} from '../model/SystemCode';
import {interval} from 'rxjs';
import {startWith, switchMap} from 'rxjs/operators';
import {PersistenceService, StorageType} from 'angular-persistence';

@Component({
  selector: 'app-systemcode',
  templateUrl: './systemcode.component.html',
  styleUrls: ['./systemcode.component.scss']
})
export class SystemcodeComponent implements OnInit {

  systemCodes: SystemCode[] = [];
  systemCodesPersistenceKey = 'systemCodes';

  constructor(private machineService: MachineService, private persistenceService: PersistenceService) {
  }

  ngOnInit() {
    interval(5000)
      .pipe(
        startWith(0),
        switchMap(() => this.machineService.getSystemCodes())
      ).subscribe(
      response => this.parseSystemCodes(response)
    );
  }

  parseSystemCodes(systemCodes: SystemCode[]) {
    systemCodes.forEach(systemCode => {
      if (!this.isSystemCodeInArray(systemCode)) {
        this.machineService.getActivitiesForSystemCode(systemCode.code).subscribe(response => {
          systemCode.activities = response;
        });
        this.systemCodes.push(systemCode);
      }
    });

    this.persistenceService.set(this.systemCodesPersistenceKey, JSON.stringify(this.systemCodes), {type: StorageType.SESSION});
  }

  private isSystemCodeInArray(systemCode: SystemCode): boolean {
    return this.systemCodes.some((sc) => sc.code === systemCode.code);
  }
}
