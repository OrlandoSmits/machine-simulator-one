import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SystemcodeComponent} from './systemcode.component';
import {SystemCodeEntryComponent} from './systemcode-entry/system-code-entry.component';
import {MaterialModule} from '../material.module';
import {MachineService} from '../service/machine/machine.service';
import {HttpClientModule} from '@angular/common/http';
import {PersistenceService} from 'angular-persistence';

describe('SystemcodeComponent', () => {
  let component: SystemcodeComponent;
  let fixture: ComponentFixture<SystemcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, HttpClientModule],
      declarations: [SystemcodeComponent, SystemCodeEntryComponent],
      providers: [MachineService, PersistenceService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
