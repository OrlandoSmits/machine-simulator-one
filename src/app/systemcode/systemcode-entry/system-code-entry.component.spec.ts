import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SystemCodeEntryComponent} from './system-code-entry.component';
import {MaterialModule} from '../../material.module';
import {MachineService} from '../../service/machine/machine.service';
import {HttpClientModule} from '@angular/common/http';

describe('SystemCodeEntryComponent', () => {
  let component: SystemCodeEntryComponent;
  let fixture: ComponentFixture<SystemCodeEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, HttpClientModule],
      declarations: [SystemCodeEntryComponent],
      providers: [MachineService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemCodeEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
