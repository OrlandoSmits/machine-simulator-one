import {Component, Input, OnInit} from '@angular/core';
import {SystemCode} from '../../model/SystemCode';
import {MachineService} from '../../service/machine/machine.service';

@Component({
  selector: 'app-systemcode-entry',
  templateUrl: './system-code-entry.component.html',
  styleUrls: ['./system-code-entry.component.scss']
})
export class SystemCodeEntryComponent implements OnInit {

  @Input()
  systemCodeEntry: SystemCode;

  constructor(private machineService: MachineService) {
  }

  ngOnInit() {
  }

  runActivity(exec: string) {
    this.machineService.runActivity(exec).subscribe();
  }
}
