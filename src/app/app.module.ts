import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {NavComponent} from './nav/nav.component';
import {LayoutModule} from '@angular/cdk/layout';
import {HttpClientModule} from '@angular/common/http';
import {MachineService} from './service/machine/machine.service';
import {MachineLogComponent} from './machine-log/machine-log.component';
import {MachineLogEntryComponent} from './machine-log/machine-log-entry/machine-log-entry.component';
import {SystemcodeComponent} from './systemcode/systemcode.component';
import {SystemCodeEntryComponent} from './systemcode/systemcode-entry/system-code-entry.component';
import {MachineDashboardComponent} from './machine-dashboard/machine-dashboard.component';
import {PersistenceModule} from 'angular-persistence';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    MachineLogComponent,
    MachineLogEntryComponent,
    SystemcodeComponent,
    SystemCodeEntryComponent,
    MachineDashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    HttpClientModule,
    PersistenceModule
  ],
  providers: [
    MachineService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
