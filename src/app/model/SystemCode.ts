import {Activity} from './Activity';

export class SystemCode {
  code: string;
  activities: Activity[];
}
