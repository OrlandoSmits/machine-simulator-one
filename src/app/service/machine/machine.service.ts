import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Machine} from '../../model/Machine';
import {Log} from '../../model/Log';
import {SystemCode} from '../../model/SystemCode';
import {Activity} from '../../model/Activity';
import {RunRequest} from './request/RunRequest';
import {RunActivityResponse} from './response/RunActivityResponse';

const machinesUrl = 'http://localhost:9000/machine';
const systemCodesUrl = 'http://localhost:9000/content/systemcodes';
const resetStateAllMachinesUrl = 'http://localhost:9000/drivers/all/reset';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class MachineService {

  constructor(private http: HttpClient) {
  }

  getAllMachines(): Observable<Machine[]> {
    return this.http.get<Machine[]>(machinesUrl);
  }

  getLogsForMachine(machineId: string): Observable<Log[]> {
    return this.http.get<Log[]>(machinesUrl + '/' + machineId + '/observelog');
  }

  getSystemCodes(): Observable<SystemCode[]> {
    return this.http.get<SystemCode[]>(systemCodesUrl);
  }

  getActivitiesForSystemCode(systemCode: string): Observable<Activity[]> {
    return this.http.get<Activity[]>(systemCodesUrl + '/' + systemCode);
  }

  runActivity(exec: string): Observable<RunActivityResponse> {
    const runRequest = new RunRequest();
    runRequest.exec = exec;
    return this.http.post<RunActivityResponse>((machinesUrl + '/1001/run'), runRequest, httpOptions);
  }

  resetStateAllMachines() {
    return this.http.delete(resetStateAllMachinesUrl);
  }
}

