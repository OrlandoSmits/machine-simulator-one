import {Component, OnInit} from '@angular/core';
import {MachineService} from '../service/machine/machine.service';
import {Log} from '../model/Log';
import {interval} from 'rxjs';
import {startWith, switchMap} from 'rxjs/operators';
import {PersistenceService, StorageType} from 'angular-persistence';

@Component({
  selector: 'app-machine-log',
  templateUrl: './machine-log.component.html',
  styleUrls: ['./machine-log.component.scss']
})
export class MachineLogComponent implements OnInit {

  logEntries: Log[] = [];

  // TODO: Fix the machine id. Needs to get from the route.
  machineId = 1001;

  constructor(private machineService: MachineService, private persistenceService: PersistenceService) {
  }

  ngOnInit() {
    interval(5000)
      .pipe(
        startWith(0),
        switchMap(() => this.machineService.getLogsForMachine(this.machineId.toString()))
      ).subscribe(
      response => this.parseLogs(response)
    );
  }

  parseLogs(logs: Log[]) {
    const jsonLogs = this.persistenceService.get(this.machineId.toString(), StorageType.SESSION);

    if (jsonLogs != null) {
      this.logEntries = JSON.parse(jsonLogs);
    }

    logs.forEach(log => {
      const parsedLog = this.parseLog(log);
      if (!this.isLogInArray(parsedLog)) {
        this.logEntries.push(parsedLog);
      }
    });

    this.persistenceService.set(this.machineId.toString(), JSON.stringify(this.logEntries), {type: StorageType.SESSION});
  }

  private isLogInArray(log: Log): boolean {
    return this.logEntries.some((l) => l.logTime === log.logTime);
  }

  private parseLog(logEntry: Log): Log {
    if (logEntry != null) {
      logEntry.logDate = logEntry.line.slice(0, 10);
      logEntry.logTime = logEntry.line.slice(11, 19);
      const restString = logEntry.line.slice(19);
      logEntry.code = restString.slice(1, 7);
      logEntry.description = restString.slice(8);

      return logEntry;
    }
  }
}
