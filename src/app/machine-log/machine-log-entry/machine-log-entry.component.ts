import {Component, Input, OnInit} from '@angular/core';
import {Log} from '../../model/Log';

@Component({
  selector: 'app-log-entry',
  templateUrl: './machine-log-entry.component.html',
  styleUrls: ['./machine-log-entry.component.scss']
})
export class MachineLogEntryComponent implements OnInit {

  @Input()
  logEntry: Log;

  constructor() {
  }

  ngOnInit() {
    this.parseLog();
  }

  parseLog(): void {
    if (this.logEntry != null) {
      this.logEntry.logDate = this.logEntry.line.slice(0, 11);
      this.logEntry.logTime = this.logEntry.line.slice(10, 19);
      const restString = this.logEntry.line.slice(19);
      this.logEntry.code = restString.slice(1, 7);
      this.logEntry.description = restString.slice(7);
    }
  }

}
