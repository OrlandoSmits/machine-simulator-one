import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MachineLogComponent} from './machine-log.component';
import {MachineLogEntryComponent} from './machine-log-entry/machine-log-entry.component';
import {MaterialModule} from '../material.module';
import {MachineService} from '../service/machine/machine.service';
import {HttpClientModule} from '@angular/common/http';
import {PersistenceService} from 'angular-persistence';

describe('MachineLogComponent', () => {
  let component: MachineLogComponent;
  let fixture: ComponentFixture<MachineLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineLogComponent, MachineLogEntryComponent],
      imports: [MaterialModule, HttpClientModule],
      providers: [MachineService, PersistenceService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
