import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MachineLogComponent} from './machine-log/machine-log.component';

const routes: Routes = [
  {path: 'logs/:id', component: MachineLogComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
