import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {NavComponent} from './nav/nav.component';
import {MaterialModule} from './material.module';
import {MachineDashboardComponent} from './machine-dashboard/machine-dashboard.component';
import {MachineLogComponent} from './machine-log/machine-log.component';
import {SystemcodeComponent} from './systemcode/systemcode.component';
import {MachineLogEntryComponent} from './machine-log/machine-log-entry/machine-log-entry.component';
import {SystemCodeEntryComponent} from './systemcode/systemcode-entry/system-code-entry.component';
import {MachineService} from './service/machine/machine.service';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PersistenceService} from 'angular-persistence';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
      declarations: [
        AppComponent,
        NavComponent,
        MachineDashboardComponent,
        MachineLogComponent,
        SystemcodeComponent,
        SystemCodeEntryComponent,
        MachineLogEntryComponent
      ],
      providers: [MachineService, PersistenceService]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
