import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {interval, Observable} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {MachineService} from '../service/machine/machine.service';
import {Machine} from '../model/Machine';
import {PersistenceService, StorageType} from 'angular-persistence';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  machines: Machine[] = [];
  machinePersistenceKey = 'machines';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private machineService: MachineService,
              private persistenceService: PersistenceService) {
  }

  ngOnInit() {
    interval(5000)
      .pipe(
        startWith(0),
        switchMap(() => this.machineService.getAllMachines())
      ).subscribe(
      response => this.parseMachines(response)
    );
  }

  parseMachines(machines: Machine[]): void {
    const jsonMachines = this.persistenceService.get(this.machinePersistenceKey, StorageType.SESSION);

    if (jsonMachines != null) {
      this.machines = JSON.parse(jsonMachines);
    }

    machines.forEach(machine => {
      if (!this.isMachineInArray(machine)) {
        this.machines.push(machine);
      }
    });

    if (this.machines.length !== 0) {
      this.persistenceService.set(this.machinePersistenceKey, JSON.stringify(this.machines), {type: StorageType.SESSION});
    }
  }

  private isMachineInArray(machine: Machine): boolean {
    return this.machines.some((m) => m.name === machine.name);
  }

  resetStateAllMachines() {
    this.machineService.resetStateAllMachines().subscribe();
  }
}
