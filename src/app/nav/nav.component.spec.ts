import {LayoutModule} from '@angular/cdk/layout';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {NavComponent} from './nav.component';
import {MachineDashboardComponent} from '../machine-dashboard/machine-dashboard.component';
import {MaterialModule} from '../material.module';
import {MachineService} from '../service/machine/machine.service';
import {RouterModule} from '@angular/router';
import {MachineLogComponent} from '../machine-log/machine-log.component';
import {SystemcodeComponent} from '../systemcode/systemcode.component';
import {MachineLogEntryComponent} from '../machine-log/machine-log-entry/machine-log-entry.component';
import {SystemCodeEntryComponent} from '../systemcode/systemcode-entry/system-code-entry.component';
import {HttpClientModule} from '@angular/common/http';
import {PersistenceService} from 'angular-persistence';

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavComponent,
        MachineDashboardComponent,
        MachineLogComponent, SystemcodeComponent,
        MachineLogEntryComponent,
        SystemCodeEntryComponent],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MaterialModule,
        RouterModule,
        HttpClientModule
      ],
      providers: [MachineService, PersistenceService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
