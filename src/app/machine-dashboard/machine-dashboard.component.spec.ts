import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MachineDashboardComponent} from './machine-dashboard.component';
import {MachineLogComponent} from '../machine-log/machine-log.component';
import {SystemcodeComponent} from '../systemcode/systemcode.component';
import {MachineLogEntryComponent} from '../machine-log/machine-log-entry/machine-log-entry.component';
import {SystemCodeEntryComponent} from '../systemcode/systemcode-entry/system-code-entry.component';
import {MaterialModule} from '../material.module';
import {MachineService} from '../service/machine/machine.service';
import {HttpClientModule} from '@angular/common/http';
import {PersistenceService} from 'angular-persistence';

describe('MachineDashboardComponent', () => {
  let component: MachineDashboardComponent;
  let fixture: ComponentFixture<MachineDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineDashboardComponent,
        MachineLogComponent,
        SystemcodeComponent,
        MachineLogEntryComponent,
        SystemCodeEntryComponent],
      imports: [MaterialModule, HttpClientModule],
      providers: [MachineService, PersistenceService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
